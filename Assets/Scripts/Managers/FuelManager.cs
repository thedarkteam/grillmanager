﻿using UnityEngine;
using System.Collections;
[SerializeField]
public class FuelManager : ScriptableObject {

    public Fuel fuelType;
    public int number;

    public float currentBurn;
    public float areBurning=0;
    public float myBurnPower;
    public float burnPower;

    public int currentNumber;

	// Use this for initialization
	void Start () {

	}
	
    public FuelManager(Fuel fuel)
    {
        fuelType = fuel;
    }


	// Update is called once per frame
	public void update ()
    {
        if (currentNumber==0)
        {
            currentBurn = 0;
        }
	    while (currentBurn>fuelType.burnNeeded&&currentNumber>0)
        {
            areBurning += 1;
            currentNumber -= 1;
            burnPower += fuelType.burnPower;
            currentBurn -= fuelType.burnNeeded;

        }

        myBurnPower = areBurning * fuelType.burnPerFuel < fuelType.MaxBurning ? areBurning * fuelType.burnPerFuel : fuelType.MaxBurning;


        burnPower -= myBurnPower;
        while (burnPower<=(areBurning-1)*fuelType.burnPower)
        {
            areBurning -= 1;
        }
    }

    public void Add (int howMany)
    {
        currentNumber += howMany;
        number += howMany;

    }


    public void AddBurn(float howMuch)
    {
        currentBurn += howMuch;
    }
}
