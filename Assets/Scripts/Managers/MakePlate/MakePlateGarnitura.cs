﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakePlateGarnitura : MonoBehaviour {

    GameState gameState;
    public GameObject textPrefab;
    public GameObject buttonPrefab;
    public GameObject garnituraPrefab;
    // Use this for initialization
    void Start()
    {
        gameState = FindObjectOfType<GameState>();

        foreach (ItemType item in gameState.myInventory.Items)
        {
            if (item.item.GetItemType() == "Garnitura")
            {
                GameObject garnitura = Instantiate(garnituraPrefab);
                garnitura.GetComponent<Image>().sprite = item.item.image;
                garnitura.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(59, 51);
                GameObject carneText = Instantiate(textPrefab);
                carneText.transform.position = Vector3.zero;
                carneText.transform.SetParent(garnitura.transform);
                garnitura.transform.SetParent(transform);
                carneText.GetComponent<Text>().text = item.number.ToString();
                carneText.GetComponent<RectTransform>().localPosition = new Vector3(0, -40f);
                GameObject garnituraButton = Instantiate(buttonPrefab);
                garnituraButton.transform.SetParent(garnitura.transform);
            }
        }
    }


    // Update is called once per frame
    void Update()
    {

    }
}
