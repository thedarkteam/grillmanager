﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MakePlateMeat : MonoBehaviour {

    GameState gameState;
    public GameObject textPrefab;
    public Button buttonPrefab;
    Button meatButton;
    public GameObject carnePrefab;

	// Use this for initialization
	void Start ()
    {
        gameState = FindObjectOfType<GameState>();

        foreach(Item item in gameState.mySellingState.takenMeat)
        {

            GameObject carne = Instantiate(carnePrefab);
            carne.GetComponent<Image>().sprite = item.image;
            carne.GetComponent<Image>().rectTransform.sizeDelta = new Vector2(59,45);
            //GameObject carneText = Instantiate(textPrefab);
            //carneText.transform.position = Vector3.zero;
            //carneText.transform.SetParent(carne.transform);
            carne.transform.SetParent(transform);
            //carneText.GetComponent<RectTransform>().localPosition = new Vector3(0, -40f);
            meatButton = Instantiate(buttonPrefab);
            meatButton.transform.SetParent(carne.transform);

        }
    }
	

	// Update is called once per frame
	void Update ()
    {
        //meatButton.onClick.AddListener(Select);
	}

    void Select()
    {
        
    }
}
