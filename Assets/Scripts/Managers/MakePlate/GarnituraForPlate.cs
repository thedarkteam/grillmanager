﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarnituraForPlate : MonoBehaviour {

    public void InverseSelection()
    {
        if (!selected)
        {
            DeleteOtherSelections();

            selected = true;
        }
        else
        {
            selected = false;
        }

    }

    public void DeleteOtherSelections()
    {
        foreach (GarnituraForPlate garnituraForPlate in FindObjectsOfType<GarnituraForPlate>())
        {
            garnituraForPlate.selected = false;
        }
    }

    public bool selected = false;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}
