﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour {


    [HideInInspector]
    public bool someoneMeatIsActive;


    public SellingState mySellingState;

    public Grill gril;

    public Inventory myInventory;

    public Sprite[] MeatSprites;

    public Meat meat;
    public Meat otherMeat;
    public Igniter igniter;
    public Coal coal;
    public Wood wood;
    public FireStarter paper;
    public Garnitura potatoes;
    public Garnitura salata;

    bool isCreated;
    public bool canBeLupa;
    private GameObject[] ObjectsToInstantiateWhenSelectedMeat;

    Text textForTakenMeat;

    void OnLevelWasLoaded()
    {
        if (GameObject.Find("MeatNumber"))
        {
            textForTakenMeat = GameObject.Find("MeatNumber").GetComponent<Text>();
        }
        if (Application.loadedLevel == 3)
        {

            isCreated = false;
                int i = 0;
                foreach (InstantiateWhenSelected objectt in FindObjectsOfType<InstantiateWhenSelected>())
                {
                    if (!isCreated)
                {
                    ObjectsToInstantiateWhenSelectedMeat = new GameObject[FindObjectsOfType<InstantiateWhenSelected>().Length];
                    isCreated = true;
                }
                    ObjectsToInstantiateWhenSelectedMeat[i] = objectt.gameObject;
                    i++;
                }
            
        }
    }
    
    // Use this for initialization
    void Awake () {

        mySellingState = ScriptableObject.CreateInstance<SellingState>(); //new SellingState(gril);
        mySellingState.grills = new List<Grill>();
        myInventory = new Inventory();

        

        // For testing purposes
        mySellingState.grills.Add(gril);
        mySellingState.grills.Add(gril);
        
        myInventory.AddItem(igniter);
        myInventory.AddItem(wood, 25);
        myInventory.AddItem(coal, 5);
        myInventory.AddItem(paper, 50);
        myInventory.AddItem(meat, 10);
        myInventory.AddItem(otherMeat, 20);
        myInventory.AddItem(potatoes, 5);
        myInventory.AddItem(salata, 42);
        Plate plateToAdd = new Plate();

        mySellingState.myMenu = new Menu();
        plateToAdd.meat = meat;
        plateToAdd.pret = 5;
        mySellingState.myMenu.AddPlate(plateToAdd);
	}
	
	// Update is called once per frame
	void Update ()
    {
	    // Increase performance here

        if (textForTakenMeat)
        {
            textForTakenMeat.text = mySellingState.takenMeat.Count.ToString();
        }

        if(!mySellingState.initialized)
        {
            mySellingState.start();
        }

        // And here
        mySellingState.update(Time.deltaTime);


        CheckForLupa();

        
    }

    void CheckForLupa()
    {
        if (canBeLupa)
        {

            


            if (someoneMeatIsActive)
            {
                if (!ObjectsToInstantiateWhenSelectedMeat[0].activeSelf)
                    ChangeActivation(true);
                    
            }
            if (!someoneMeatIsActive)
            {

                if (ObjectsToInstantiateWhenSelectedMeat[0].activeSelf)
                    ChangeActivation(false);

            }
        }
    }

    void ChangeActivation(bool value)
    {
        int i = 0;
        foreach (GameObject obj in ObjectsToInstantiateWhenSelectedMeat)
        {
            ObjectsToInstantiateWhenSelectedMeat[i].SetActive(value);
            i++;
        }
    }


}
