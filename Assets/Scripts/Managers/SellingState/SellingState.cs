﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class SellingState : ScriptableObject {

    public Menu myMenu;

    public List<Grill> grills;

    public List<Fire> fires = new List<Fire>();
    public List<Meat> takenMeat = new List<Meat>();
    public Grill currentGrill;

    public int currentGrillId;
    public int currentId;
    public bool initialized = false;
    // Use this for initialization
    public void start()
    {

        int i = 0;

        foreach (Grill grill in grills)
        {
            fires.Add(new Fire());
            fires[i].myGrill = grill;
            fires[i].firstStageColor = 2f;
            fires[i].maxPowerColor = 100f;
            i++;
        }

        initialized = true;
    }

    public void update(float elapsedTime)
    {
        foreach(Fire fire in fires)
        {
            if (fire)
            fire.update(elapsedTime);
        }
    }

    public SellingState()
    {

    }

    public SellingState(List<Grill> grill)
    {
        grills = grill;
    }

    //public SellingState (Grill grill)
    //{
    //    List<Grill> grillss = new List<Grill>();
    //    grillss.Add(grill);
    //    grills = grillss;
    //}

	// Update is called once per frame
	void Update () {
	
	}

    public void TakeMeat(int id)
    {
        takenMeat.Add(fires[currentGrillId].Meats[id]);
        RemoveAndMakeItNull(fires[currentGrillId].Meats, id);
    }

    void RemoveAndMakeItNull(List<Meat> objs, int where)
    {
        objs[where] = null;
        if (where==objs.Count-1)
        {
            objs.RemoveAt(where);
        }
        while (objs[objs.Count-1]==null)
        {
            objs.RemoveAt(objs.Count - 1);
        }
    }

    public void RemoveMeat(Meat meat)
    {
        int i = 0;
        foreach(Meat meatt in takenMeat)
        {
            if (meat == meatt)
            {
                takenMeat.RemoveAt(i);
            }
            i++;
        }
    }

}
