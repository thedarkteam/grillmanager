﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class StuffForProperGrill : MonoBehaviour {

    public MeatState meatState;
    public GameState gameState;
    public SellingState mySellingState;
    
    public MeatObject toMove, toWho;

    public Image grillImage;

    

	// Use this for initialization
	void Start ()
    {
        meatState = MeatState.IsNotSelected;
        gameState = FindObjectOfType<GameState>();
        mySellingState = FindObjectOfType<GameState>().mySellingState;
        grillImage = FindObjectOfType<GrillImage>().GetComponent<Image>();
        mySellingState.fires[mySellingState.currentId].CoalsImage = grillImage;
    }

    public Meat GetSelectedMeat()
    {
        int id = 0;
        foreach(MeatObject obj in FindObjectsOfType<MeatObject>())
        {
            if (obj.selected)
            {
                id = obj.id;
                break;
            }
        }

        Meat meatToReturn = gameState.mySellingState.fires[gameState.mySellingState.currentGrillId].Meats[id];

        return meatToReturn;
    }
    public MeatObject GetSelectedMeatObject()
    {
        foreach (MeatObject obj in FindObjectsOfType<MeatObject>())
        {
            if (obj.selected)
            {
                return obj;
            }
        }
        return null;
    }

    public void AddMeat(Meat meat)
    {
        bool takeFromMadeMeat = FindObjectOfType<SpaceForGottenMeat>().selected;

        
        if (takeFromMadeMeat && mySellingState.takenMeat.Count<1)
        {
            Message.Print("Not enough meat to take");
            return;
        }

        if (takeFromMadeMeat)
        {
            meat = mySellingState.takenMeat[0];
        }

        if (gameState.mySellingState.fires[gameState.mySellingState.currentId].IsNotOccupied())
        {
            if (gameState.myInventory.GetNumber(meat) > 0)
            {

                if (GetSelectedMeatObject()!=null)
                {
                    if (!GetSelectedMeatObject().IsMeatHere())
                    {
                        int id = GetSelectedMeatObject().id;
                        mySellingState.fires[mySellingState.currentGrillId].AddMeatAt(meat,id);


                    }
                    else
                    {
                    mySellingState.fires[mySellingState.currentGrillId].AddMeat(meat);
                    }
                }
                else
                {
                    mySellingState.fires[mySellingState.currentGrillId].AddMeat(meat);
                }


                if (takeFromMadeMeat)
                {
                    mySellingState.takenMeat.RemoveAt(0);
                }
                else
                {
                    gameState.myInventory.RemoveItem(meat);
                }
            }
            else
            {
                Message.Print("No more meat...");
            }
        }
        else
        {
            Message.Print("No more space...");
        }
    }

    public void Ignite(Igniter igniter)
    {

        mySellingState.fires[mySellingState.currentGrillId].Ignite(igniter);
    }

    public void MoveMeat()
    {
        int aux;
        aux = toMove.id;
        toMove.id = toWho.id;
        toWho.id = aux;
    }

    public void AddFuel(Fuel fuel)
    {
        if (gameState.myInventory.GetNumber(fuel) > 0)
        {
            mySellingState.fires[mySellingState.currentId].AddFuel(fuel);
            gameState.myInventory.RemoveItem(fuel);
        }
        else
        {
            Message.Print("Not enough " + fuel.Name,1f,2f);
        }
    }

    public void TakeSelectedMeat()
    {
        int id;
        id = GetSelectedMeatObject().id;
        GetSelectedMeatObject().Clicked();
        mySellingState.TakeMeat(id);
    }

}
