﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : ScriptableObject {

    public List<Plate> plateList = new List<Plate>();
    public int maxListCap;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        plateList.Capacity = maxListCap;
	}

    public void AddPlate(Plate plate)
    {
        plateList.Add(plate);
    }
}
