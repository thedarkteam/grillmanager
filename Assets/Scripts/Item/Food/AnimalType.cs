﻿﻿using UnityEngine;
using System.Collections;

public enum AnimalType
{

	Cow,
    Chicken,
    Pork,
    Dog,
    Horse,
    Cat
}