﻿using UnityEngine;
using System.Collections;

public enum BodyPart
{

    Tail,
    Chest,
    Neck,
    Leg   
	
}
