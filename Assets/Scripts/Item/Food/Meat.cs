﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu()]
public class Meat : Item {

    public AnimalType myType;
    public BodyPart myBody;

    public float weight;

    public float flavour;

    public float[] burnOnFaces;

    public int currentFace=0;

    // Use this for initialization
    public void start()
    {
        InitializeFaces();
    }

    public override string GetItemType()
    {
        return "Meat";
    }

    // Update is called once per frame
    void Update() {

    }

    void InitializeFaces()

    {
        flavour = 0;
        int i = 0;
        switch (myBody)
        {
            case BodyPart.Chest:
                i = 2;
                break;

            case BodyPart.Leg:
                i = 4;
                break;

            case BodyPart.Neck:
                i = 2;
                break;

            case BodyPart.Tail:
                i = 3;
                break;

            default:
                i = 4;
                break;
        }

        burnOnFaces = new float[i];
    }

    public void AddBurn(float much)
    {
        burnOnFaces[currentFace] += much;
    }

    public void Rotate()
    {
        currentFace += 1;
        if (currentFace>burnOnFaces.Length)
        {
            currentFace = 0;
        }
    }

}
