﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu()]
public class Wood : Fuel {

    public override string GetItemType()
    {
        return "Wood";
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
