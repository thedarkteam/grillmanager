﻿using UnityEngine;
using System.Collections;
using System;

public abstract class Fuel : Item {

    public override string GetItemType()
    {
        return "Fuel";
    }

    public float burnPower;
    public float burnPerFuel;
    public float flavour;
    public float MaxBurning;
    public float burnNeeded;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
