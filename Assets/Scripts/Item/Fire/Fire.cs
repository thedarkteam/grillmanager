﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Fire : ScriptableObject {
    public float FryingPower;
    public Grill myGrill;
    public Image CoalsImage;
    public float maxPowerColor = 100;
    public float firstStageColor = 2f;
    public float power;
    public float powerFromIgniter;
    public int totalNumber;
    public List<FuelManager> FuelManagers = new List<FuelManager>();
    public List<Meat> Meats = new List<Meat>();
    float bigTimer;
    float timer;
    public int MaxMeatCount = 0;
    public float totalBurning;
    public TextScrip textscrip;
    public float timeModifier = 0.5f;
    // Use this for initialization
    void Start()
    {

        Meats = new List<Meat>(6);

    }

    public bool IsNotOccupied()
    {
        for (int i =0; i < Meats.Count;i++)
        {
            if (Meats[i]==null)
            {
                return true;
            }
        }

        if (Meats.Count<myGrill.space)
        {
            return true;
        }
        else
        return false;
    }

    // Update is called once per frame

    public void update(float elapsedTime)
    {
        timer += elapsedTime;


        if (CoalsImage)
        {
            if (power < 0.5 && power > 0)
            {
                CoalsImage.color = Color.Lerp(Color.white, Color.yellow, (float)power / 0.5f);
            }

            if (power > 0.5f)
            {
                CoalsImage.color = Color.Lerp(Color.yellow, Color.red, power / 5f);
            }
        }

        while (bigTimer > 1f)
        {
            UpdateFryingPower();
            bigTimer -= 1f;

        }

        while (timer >= 0.2f)
        {
            totalBurning = 0;
            totalNumber = 0;

            if (powerFromIgniter > 0)
            {
                powerFromIgniter -= myGrill.powerLoss * timeModifier;
                if (powerFromIgniter < 0)
                    powerFromIgniter = 0;
            }
            power = powerFromIgniter;
            foreach (FuelManager fuelManager in FuelManagers)
            {
                power += fuelManager.myBurnPower;
                totalNumber += fuelManager.currentNumber;

            }

            foreach (FuelManager fuelManager in FuelManagers)
            {
                if (totalNumber > 0)
                {
                    fuelManager.AddBurn(power * fuelManager.currentNumber / totalNumber);

                }
                fuelManager.update();
                totalBurning += fuelManager.burnPower;

            }

            foreach (Meat meat in Meats)
            {
                if (meat)
                meat.AddBurn(power);
            }




            timer -= 0.2f;
            bigTimer += 0.2f;

        }
    }



    public void UpdateFryingPower()
    {
        FryingPower = power;
    }

    public void Ignite(Igniter igniter)
    {
        if (igniter.uses > 0)
        {
            if (powerFromIgniter < igniter.basePower / 2)
            {
                powerFromIgniter = igniter.basePower;
            }
            else if (powerFromIgniter < igniter.basePower * 2)
            {
                powerFromIgniter += igniter.basePower / 4;
            }
            igniter.uses--;
        }
    }

    public int MeatHighestNumber()
    {
        return MaxMeatCount;
    }

    public void AddMeatAt(Meat meat, int index)
    {
        bool[] needsToBeDeleted = new bool[index];

        int i = 0;

       
        if (index<Meats.Count)
        {
            Meats[index] = meat;
            return;
        }
        while (i<index)
        {
            if (i<Meats.Count)
            {
                if (Meats[i] != null)
                {
                    needsToBeDeleted[i] = false;
                }
            }
            else
            {
                Meats.Add(meat);
                needsToBeDeleted[i] = true;
            }
          
            i++;
        }
        Meats.Add(meat);
        Meats[Meats.Count - 1].start();
        i--;
        while (i>-1)
        {
            if (needsToBeDeleted[i])
            {
                Meats[i] = null;
            }
            i--;
        }
        
    }

    public void AddMeat(Meat meat)
    {
        if (meat == null)
        {
            Meats.Add(meat);
            return;
        }

        bool needsSomething = true;
        for (int i =0; i <Meats.Count; i++)
        {
            if (Meats[i]==null)
            {
                Meats[i] = meat;
                Meats[i].start();

                needsSomething = false;
                break;
            }
        }
        if (needsSomething)
        {
            Meats.Add(meat);
            Meats[Meats.Count - 1].start();

        }

        if (Meats.Count > MaxMeatCount)
        {
            MaxMeatCount = Meats.Count;
        }
    }

    public void AddFuel(Fuel fuel, int howMany)
    {
        foreach (FuelManager fuelManager in FuelManagers)
        {
            if (fuel == fuelManager.fuelType)
            {
                fuelManager.Add(howMany);
                return;
            }
        }
        FuelManagers.Add(new FuelManager(fuel));
        FuelManagers[FuelManagers.Count-1].Add(howMany);
    }

    public void AddFuel (Fuel fuel)
    {
        AddFuel(fuel, 1);
    }


}
