﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu()]
public class Igniter : Item {

    public override string GetItemType()
    {
        return "Igniter";
    }

    public float basePower;
    public float maxPower;
    public int uses;
}
