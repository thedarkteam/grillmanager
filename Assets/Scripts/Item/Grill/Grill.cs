﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu()]
public class Grill : Item {

    public override string GetItemType()
    {
        return "Grill";
    }

    public float MaxIntenisty;
    public float Durability;
    public float DamageLevel;
    public float FilthinessLevel;
    public float powerLoss;
    public int space;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
