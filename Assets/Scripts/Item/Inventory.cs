﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : ScriptableObject {

     public List<ItemType> Items = new List<ItemType>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    /// <summary>
    /// Add some items to the inventory
    /// </summary>
    /// <param name="item">The type of item you want to add</param>
    /// <param name="number">How many items of that type to add</param>
    public void AddItem (Item item, int number)
    {
        bool found = false; 
        foreach (ItemType itemType in Items)
        {
            if(item == itemType.item)
            {
                itemType.number += number;
                found = true;
                return;
            }
        }

        if (!found)
        {
            Items.Add(new ItemType());
            Items[Items.Count - 1].item = item;
            Items[Items.Count - 1].number = number;
        }

    }

    public int GetNumber (Item item)
    {
        foreach (ItemType itemType in Items)
        {
            if (item ==itemType.item)
            {
                return itemType.number;
            }
        }
        return 0;
    }

    public void AddItem(Item item)
    {
        AddItem(item, 1);
    }

    /// <summary>
    /// Remove some items from inventory
    /// </summary>
    /// <param name="item">The item you want to remove</param>
    /// <param name="number">How many</param>
    /// <returns>Returns true if succesfull, false if there are not enough items</returns>
    public bool RemoveItem(Item item, int number)
    {
        bool found = false;
        foreach(ItemType itemType in Items)
        {
            if (item == itemType.item)
            {
                if (itemType.number>=number)
                {
                    itemType.number -= number;
                    found = true;
                    return true;
                }
                else
                {
                    found = true;
                    return false;
                }
            }
        }

        if (!found)
            {
                return false;
            }

        return false;

    }


    public bool RemoveItem (Item item)
    {
        return RemoveItem(item, 1);
    }

}
