﻿using UnityEngine;

public class ItemType : ScriptableObject {

    public Item item;
    public int number;

}
