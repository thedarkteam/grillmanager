﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeatObject : MonoBehaviour {

    public int id;
    public bool selected;
    public SellingState currentSellingState;
    public GameState gameState;
    Color transparent;
    Color semiTransparentYellow;
    public Image myImage;
    public Image backgroundImage;
    public Button button;
    // Use this for initialization
    void Start()
    {
        gameState = FindObjectOfType<GameState>();
        currentSellingState = gameState.mySellingState;
        button = transform.parent.GetComponent<Button>();
        myImage = gameObject.GetComponent<Image>();
        backgroundImage = transform.parent.GetComponent<Image>();
        button.targetGraphic = myImage;
        transparent = new Color(0, 0, 0, 0);
        semiTransparentYellow = Color.yellow;
    }


    public bool IsMeatHere()
    {
        if (IsPossibleToBeMeatHere())
            if (currentSellingState.fires[currentSellingState.currentGrillId].Meats[id] != null)
            {
                return true;
            }
        return false;
    }

    bool IsPossibleToBeMeatHere()
    {
        if (id < currentSellingState.fires[currentSellingState.currentGrillId].Meats.Count)
        {
            return true;
        }
        return false;
    }

    Sprite FindGoodSpriteForMeat()
    {
        int id=0;
        switch (currentSellingState.fires[currentSellingState.currentId].Meats[this.id].Name)
        {

            case "Chicken Leg":
                id = 0;
                break;
            case "Cat Tail":
                id = 1;
                break;
        }



        return FindObjectOfType<GameState>().MeatSprites[id];

    }

    // Update is called once per frame
    void Update() {
        if (selected)
        {
            if (IsMeatHere())
            {
                gameState.someoneMeatIsActive = true;
            }
            else
            {
                gameState.someoneMeatIsActive = false;
            }
        }


        if (selected)
        {
            backgroundImage.color = semiTransparentYellow;
        }
        if (!selected)
        {
            backgroundImage.color = transparent;
        }

        if (IsPossibleToBeMeatHere())
        {
            if (IsMeatHere())
            {
                if (!gameObject.GetComponent<Image>())
                {
                    myImage = gameObject.AddComponent<Image>();
                    myImage.color = Color.white;
                }
                else
                    myImage = gameObject.GetComponent<Image>();

                if (currentSellingState.fires[currentSellingState.currentId].Meats[id])
                {
                    // To be changed when there are multiple meat sprites
                    Sprite ImageToHave = FindGoodSpriteForMeat();

                    if (myImage.sprite != ImageToHave)
                    {
                        myImage.sprite = ImageToHave;
                        myImage.color = Color.white;
                        myImage.rectTransform.rotation = CreateARandomRotation();
                    }

                    ChangeColorOfMeat();


                }

                //button.targetGraphic = myImage;

            }
            else
            {
                if (myImage)
                {
                    myImage.sprite = null;
                    myImage.color = transparent;
                }
            }
        }
        else
        {
            if (myImage)
            {
                myImage.sprite = null;
                myImage.color = transparent;
            }
        }

    }

    void ChangeColorOfMeat()
    {

    }

    Quaternion CreateARandomRotation()
    {
        Quaternion quaternion = new Quaternion();

        quaternion.SetEulerAngles(0f, 0f, Random.Range(0, 360));

        return quaternion;
    }


    public void Clicked()
    {

        if (!selected)
        {
            if (FindObjectOfType<StuffForProperGrill>().meatState == MeatState.IsSelected)
            {
                selected = true;

                FindObjectOfType<StuffForProperGrill>().AddMeat(null);
                DeleteOtherSelections();
                FindObjectOfType<SpaceForGottenMeat>().ChangeSelection(false);
                selected = false;

            }
            else
            {
                DeleteOtherSelections();
                FindObjectOfType<SpaceForGottenMeat>().ChangeSelection(false);
                selected = true;
            }
            
        }
        else
        {
            selected = false;
            gameState.someoneMeatIsActive = false;
        }

    
    }

    public static void DeleteOtherSelections()
    {
        Transform Parent = FindObjectOfType<GrillImage>().transform;
        foreach (Transform child in Parent)
        {
            child.GetChild(0).gameObject.GetComponent<MeatObject>().selected = false;
        }
        //FindObjectOfType<SpaceForGottenMeat>().ChangeSelection(false);
    }

}
