﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceForGottenMeat : MonoBehaviour {

    Color startColor;
    public Color selectedColor = Color.yellow;
    public bool selected;

    public void InverseSelection()
    {
        if(selected)
        {
            ChangeSelection(false);
        }
        else

        {
            ChangeSelection(true);
        }
    }

    public void ChangeSelection(bool isTrue)
    {
        bool isFalse = !isTrue;
        Image childImage = transform.GetChild(0).GetComponent<Image>();
        if (selected)
        {
            if (isTrue)
            {
                //MeatObject.DeleteOtherSelections();
                return;
            }

            if (isFalse)
            {
                selected = false;
                FindObjectOfType<StuffForProperGrill>().meatState = MeatState.IsNotSelected;
                childImage.color = startColor;
            }

        }

        if (!selected)
        {
            if (isFalse)
            {
                return;
            }

            if (isTrue)
            {
                //MeatObject.DeleteOtherSelections();
                MeatObject.DeleteOtherSelections();

                selected = true;
                FindObjectOfType<StuffForProperGrill>().meatState = MeatState.IsSelected;
                childImage.color = selectedColor;
            }
        }

    }


	// Use this for initialization
	void Start () {
        startColor = transform.GetChild(0).GetComponent<Image>().color;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
