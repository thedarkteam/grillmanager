﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasScript : MonoBehaviour {

    Camera camera;

	// Use this for initialization
	void Start ()
    {
        camera = FindObjectOfType<Camera>();
        GetComponent<Canvas>().worldCamera = camera;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
