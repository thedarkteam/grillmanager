﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Message
{

    //Exist only one for this class
    static GameObject lastMessage;
    
    public static void Print(string message, float startFadeDuration, float duration)
    {
        if (lastMessage)
        {
            GameObject.Destroy(lastMessage);
        }
        GameObject myObject;
        myObject = new GameObject();
        myObject.transform.position = GameObject.FindObjectOfType<Canvas>().transform.position + Vector3.up*50f;
        myObject.transform.SetParent(GameObject.FindObjectOfType<Canvas>().transform);
        myObject.AddComponent<Text>().text = message;
        myObject.GetComponent<RectTransform>().sizeDelta = new Vector2(177f,50f);
        myObject.GetComponent<Text>().color = Color.red;
        myObject.GetComponent<Text>().font = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
        myObject.AddComponent<FadeText>().startFadeDuration = startFadeDuration;
        myObject.GetComponent<FadeText>().duration = duration;
        lastMessage = myObject;
    }
	
    public static void Print(string message)
    {
        Print(message, 1.5f, 3f);
    }



}
