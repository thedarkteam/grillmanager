﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FadeText : MonoBehaviour {


    Text myText;
    public float duration;
    public float startFadeDuration;

    float fadeDuration;
	// Use this for initialization
	void Start () {
        fadeDuration = startFadeDuration;
        myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        duration -= Time.deltaTime;
        if (duration<=0)
        {
            fadeDuration -= Time.deltaTime;
        }
        myText.color = new Color (myText.color.r,myText.color.g,myText.color.b, Mathf.Lerp(0, 100, (float)fadeDuration / (float)startFadeDuration));
        if (fadeDuration<=0)
        {
            Destroy(gameObject);
        }
    }
}
