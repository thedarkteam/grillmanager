﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
//hello
public class TextScrip : MonoBehaviour {
    public Text myText;
    public int target;
    public string TowriteBefore;
	// Use this for initialization
	void Start () {
        myText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        myText.text = TowriteBefore + target;
	}

    public void Add()
    {
        target += 1;
    }

    public void Remove()
    {
        target -= 1;
    }

    public void Reset()
    {
        //SceneManager.LoadScene(0);
    }
}
